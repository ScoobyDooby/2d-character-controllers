using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopdownCombinedMoveAndAim : MonoBehaviour
{
    Animator animator;
    Rigidbody2D rigidbody2d;

    public float speed = 3;
    float horizontalInput;
    float verticalInput;
    Vector2 lookDirection = Vector2.down;

    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        horizontalInput = Input.GetAxisRaw("Horizontal");
        verticalInput = Input.GetAxisRaw("Vertical");

        bool moving = !Mathf.Approximately(horizontalInput, 0) || !Mathf.Approximately(verticalInput, 0);
        if (moving)
        {
            lookDirection.Set(horizontalInput, verticalInput);
            lookDirection.Normalize();
        }
        animator.SetFloat("xDir", lookDirection.x);
        animator.SetFloat("yDir", lookDirection.y);
    }

    private void FixedUpdate()
    {
        Vector2 move = new Vector2(horizontalInput, verticalInput);
        animator.SetFloat("speed", move.magnitude);

        move.Normalize();
        move *= speed * Time.deltaTime;
        Vector2 position = rigidbody2d.position;
        position += move;
        rigidbody2d.MovePosition(position);
    }
}
