// Basic script for logging joystick inputs.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoyInputLogger : MonoBehaviour
{

    public int buttonCount = 16;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // Unity input manager has up to 20 joystick buttons
        for (int i = 0; i < 19; i++)
        {
            string buttonName = "joystick button " + i;
            if (Input.GetKeyDown(buttonName))
            {
                Debug.Log(buttonName + " down.");
            }
        }


        if (Input.GetAxis("Horizontal") > 0.1f)
        {
            Debug.Log("Input Axis: Horizontal positive");
        }
        if (Input.GetAxis("Horizontal") < -0.1f)
        {
            Debug.Log("Input Axis: Horizontal negative");
        }

        if (Input.GetAxis("Vertical") > 0.1f)
        {
            Debug.Log("Input Axis: Vertical positive");
        }
        if (Input.GetAxis("Vertical") < -0.1f)
        {
            Debug.Log("Input Axis: Vertical negative");
        }

        // Unity input has up to 29 joystick axes.
        for (int i = 3; i < 28; i++)
        {
            string axisName = "JoyAxis" + i;
            if(Input.GetAxis(axisName) > 0.1f) 
            {
                Debug.Log("Input Axis: " + axisName + " positive");
            }
            if (Input.GetAxis(axisName) < -0.1f)
            {
                Debug.Log("Input Axis: " + axisName + " negative");
            }
        }

    }
}
