using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XTurnYForwardBack : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Transform bulletStart;
    public float bulletForce = 300;


    public KeyCode fireKey = KeyCode.Space;
    public KeyCode joyFireButton = KeyCode.JoystickButton0;

    public KeyCode strafeKey = KeyCode.N;
    public KeyCode joyStrafeButton = KeyCode.JoystickButton4;

    Animator animator;
    Rigidbody2D rigidbody2d;

    public float speed = 3;
    [Tooltip("Turnrate in degrees per second.")]
    public float turnRate = 45f;

    float horizontalInput;
    float verticalInput;
    Vector2 lookDirection;
    Vector2 move = Vector2.zero;
    public float angleOffset = 0f;

    bool moving = false;

    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        lookDirection = transform.up;
    }

    void Update()
    {
        horizontalInput = Input.GetAxisRaw("Horizontal");
        verticalInput = Input.GetAxisRaw("Vertical");
        bool strafing = Input.GetKey(strafeKey) || Input.GetKey(joyStrafeButton);
        
        if (strafing)
        {
            moving = !Mathf.Approximately(horizontalInput, 0) || !Mathf.Approximately(verticalInput, 0);
            if(moving)
            {
                move = transform.right * verticalInput + transform.up * -horizontalInput;
                //move.Normalize();
                move *= speed;
            }
        }
        else
        {
            bool turning = !Mathf.Approximately(horizontalInput, 0);
            moving = !Mathf.Approximately(verticalInput, 0);
            if (turning)
            {
                lookDirection = Quaternion.Euler(0, 0, -horizontalInput * turnRate * Time.deltaTime) * lookDirection;
            }
            if (moving)
            {
                move = transform.right * speed * verticalInput;
            }
            else
            {
                move = Vector2.zero;
            }
        }

        bool firing = Input.GetKeyDown(fireKey) || Input.GetKeyDown(joyFireButton);
        if (firing)
        {
            FireBullet();
        }
    }

    void FireBullet()
    {
        GameObject bulletObject = Instantiate(bulletPrefab, bulletStart.position, Quaternion.identity);

        Bullet bullet = bulletObject.GetComponent<Bullet>();
        bullet.Launch(lookDirection, bulletForce);
        Debug.Log("Firing");
        // animator.SetTrigger("Launch");
        // TODO add audio
        //audioSource.PlayOneShot(throwCogClip);
    }

    private void FixedUpdate()
    {
        if (moving)
        {
            move *= Time.deltaTime;
            animator.SetFloat("speed", move.magnitude);
            Vector2 position = rigidbody2d.position;
            position += move;
            rigidbody2d.MovePosition(position);
        }
        else
        {
            animator.SetFloat("speed", 0);
        }

        var angle = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg + angleOffset;
        rigidbody2d.rotation = angle;
    }
}
