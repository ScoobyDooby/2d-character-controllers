using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XYMoveAim : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Transform bulletStart;
    public float bulletForce = 300;

    public KeyCode fireKey = KeyCode.Space;
    public KeyCode joyFireButton = KeyCode.JoystickButton0;

    public KeyCode strafeKey = KeyCode.N;
    public KeyCode joyStrafeButton = KeyCode.JoystickButton4;

    Animator animator;
    Rigidbody2D rigidbody2d;

    public float speed = 3;
    float horizontalInput;
    float verticalInput;
    Vector2 lookDirection;
    Vector2 move = Vector2.zero;
    public float angleOffset = 0f;

    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        lookDirection = transform.right;
    }

    void Update()
    {
        horizontalInput = Input.GetAxisRaw("Horizontal");
        verticalInput = Input.GetAxisRaw("Vertical");

        bool joyStrafing = Input.GetKey(joyStrafeButton);
        bool keyStrafing = Input.GetKey(strafeKey);
        bool moving = !Mathf.Approximately(horizontalInput, 0) || !Mathf.Approximately(verticalInput, 0);
        move = Vector2.zero;

        bool strafing = (joyStrafing || keyStrafing) && moving;
        
        if(strafing)
        {
            move = new Vector2(horizontalInput, verticalInput);
        }
        else if (moving)
        {
            lookDirection.Set(horizontalInput, verticalInput);
            lookDirection.Normalize();
            move = new Vector2(horizontalInput, verticalInput);
        }

        bool firing = Input.GetKeyDown(fireKey) || Input.GetKeyDown(joyFireButton);
        if (firing)
        {
            FireBullet();
        }
    }

    void FireBullet()
    {
        GameObject bulletObject = Instantiate(bulletPrefab, bulletStart.position, Quaternion.identity);

        Bullet bullet = bulletObject.GetComponent<Bullet>();
        bullet.Launch(lookDirection, bulletForce);
        // animator.SetTrigger("Launch");
        // TODO add audio
        //audioSource.PlayOneShot(throwCogClip);
    }

    private void FixedUpdate()
    {
        animator.SetFloat("speed", move.magnitude);

        move.Normalize();
        move *= speed * Time.deltaTime;
        Vector2 position = rigidbody2d.position;
        position += move;
        rigidbody2d.MovePosition(position);

        var angle = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg + angleOffset;
        rigidbody2d.rotation = angle;
    }
}
